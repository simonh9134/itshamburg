<?php

/**
 * Extension Manager/Repository config file for ext "itshamburg".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'ITSHamburg',
    'description' => 'Sitepackage for ITS Hamburg',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.5.99',
            'fluid_styled_content' => '8.7.0-9.5.99',
            'rte_ckeditor' => '8.7.0-9.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Raikeschwertner\\Itshamburg\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Simon Huang',
    'author_email' => 'simon.huang@outlook.de',
    'author_company' => 'RAIKESCHWERTNER',
    'version' => '1.0.0',
];
