//---------------SWIPER---------------
var mySwiper = new Swiper('.swiper-container', {
  direction: 'horizontal',
  loop: true,

  autoplay: {
    delay: 5000,
  },

  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
  },

  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
})

//---------------BUTTONS---------------
const header = document.querySelector(".js-header");
const headerLogo = document.querySelector(".js-header-logo")
const navigation = document.querySelector(".js-navigation");
const search = document.querySelector(".js-search");

const menuSandwich = document.querySelector(".js-button-menutoggle");
const menuSearch = document.querySelector(".js-button-search");
const menuUp = document.querySelector(".js-button-up");

//css-classes
const navigationActive = "navigation--active";
const searchActive = "search--active";
const menuClose = "js-button-close";
const menuSearchActive = "js-button-search-grey";
const visuallyHidden = "visuallyHidden";
const headerTransparent = "header--transparent";

function toggleNavigation() {
  header.classList.toggle(headerTransparent);
  menuSandwich.classList.toggle(menuClose);
  menuSearch.classList.toggle(visuallyHidden);
  navigation.classList.toggle(navigationActive);
  headerLogo.classList.toggle(visuallyHidden);
}

function toggleSearch() {
  menuSearch.classList.toggle(menuClose);
  menuSearch.classList.toggle(menuSearchActive);
  search.classList.toggle(searchActive);

}

menuSandwich.addEventListener('click', function () {
  toggleNavigation();
})

menuSearch.addEventListener('click', function () {
  toggleSearch();
})

menuUp.addEventListener('click', function () {
  window.scrollBy({
    top: scrollPos,
    behavior: 'smooth',
  });
})

//---------------HEADER---------------
const headerHidden = "header--hidden";
const headerSmaller = "header--smaller";

var scrollPos = 0;

function headerShrink() {
  if (document.body.getBoundingClientRect().top < -100) {
    header.classList.add(headerSmaller)
  } else {
    header.classList.remove(headerSmaller)
  };
};

function headerHide() {
  if ((document.body.getBoundingClientRect()).top < scrollPos
    && header.classList.contains(headerSmaller) == true
    && navigation.classList.contains(navigationActive) == false
    && search.classList.contains(searchActive) == false
  ) {
    header.classList.add(headerHidden);
  } else {
    header.classList.remove(headerHidden);
  };
  scrollPos = (document.body.getBoundingClientRect()).top;
};

window.addEventListener('scroll', function () {
  headerShrink();
  headerHide();
});

//---------------MENULINKS---------------
const menuLink = document.querySelectorAll(".js-menu-item");

menuLink.forEach(function (item, index) {
  item.addEventListener('click', function (event) {
    if (/Edge/.test(navigator.userAgent)) {
    } else {
      event.preventDefault();
  
      var target = item.getAttribute("href");
      var targetOffset = document.querySelector(target).getBoundingClientRect();
      var targetYPosition = targetOffset.top;
  
      window.scrollBy({
        top: targetYPosition,
        behavior: 'smooth',
      });  
    }

    //hides menu
    toggleNavigation();
  });
});

//---------------ROTESBAND---------------
rotesBand = document.querySelectorAll(".rotesBand");

window.addEventListener('scroll', function () {
  rotesBand.forEach(function (e, index) {
    var rotesBandPosition = e.getBoundingClientRect().top - 500;

    if (rotesBandPosition < 5) {
      e.classList.remove("rotesBand--" + index + "--hidden");
    }
  });
});

rotesBand.forEach(function (e, index) {
  e.classList.add("rotesBand--" + index + "--hidden");
});

const videoPlayerIframe = document.querySelector(".js-video");
const videoOverlay = document.querySelector(".js-overlay");
const videoOverlayHide = "overlay--hidden";
//---------------VIDEO---------------

var tag = document.createElement('script');
tag.id = 'iframe-demo';
tag.src = 'https://www.youtube.com/iframe_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('js-video-iframe', {
  });
}


videoOverlay.addEventListener('click', function(){
  videoOverlay.classList.add(videoOverlayHide);
  player.playVideo();
})